var collapsibles = document.getElementsByClassName("collapsible");
var toggles = document.getElementsByClassName("collapsible__click");

// Tool to allow for the computer to catch up and render correctly.
function sleep(ms) 
{
	return new Promise(resolve => setTimeout(resolve, ms));
}

// Collapsible "Class"
class Collapsible
{
	constructor(node)
	{
		this.node = node;
		this.parent = null;
		this.header = null;
		this.cargo = null;
		this.isOpen = true;
		this.headerClass = null;
		
		for (var j = 0; j < node.children.length; j++)
		{
			if (this.node.children[j].classList.contains("collapsible__header"))
				this.header = this.node.children[j];
			if (this.node.children[j].classList.contains("collapsible__cargo"))
				this.cargo = this.node.children[j];
		}
		if (this.node.parentElement != null)
			if (this.node.parentElement.classList.contains("collapsible__cargo"))
				this.parent = node.parentElement;
				
		if (this.isOpen == true)
		{
			this.Open();
		}
		else
		{
			this.Close();
			this.ToggleHeader();
		}
		
		this.headerClass = "collapsible__header--open_blue";
		if (this.header.classList.contains("collapsible__header--open_green"))
			this.headerClass = "collapsible__header--open_green";
	}
	
	async ExpandParent()
	{
		await sleep(150);
		// this.parent.style.maxHeight = (this.ChildrenHeight(this.parent) + 20) + "px";
		this.parent.style.maxHeight = "";
	}
	
	ChildrenHeight(n)
	{
		var cargoHeight = 0;
		for (var i = 0; i < n.children.length; i++)
		{
			cargoHeight += n.children[i].scrollHeight;
		}
		
		return cargoHeight;
	}
	
	Toggle()
	{
		this.ToggleHeader();
		
		if (this.isOpen == true)
			this.Close();
		else
			this.Open();
	}
	
	ToggleHeader()
	{
		this.header.classList.toggle(this.headerClass);
		this.header.getElementsByClassName("collapsible__click")[0].classList.toggle("collapsible__click--rotate");
	}
	
	async Close()
	{
		this.cargo.style.maxHeight = this.ChildrenHeight(this.cargo) + "px";
		await sleep(100);
		this.cargo.style.maxHeight = "0px";
		this.cargo.style.padding = "0px";
		this.isOpen = false;
		
	}
	
	async Open()
	{
		this.cargo.style.maxHeight = (this.ChildrenHeight(this.cargo) + 20) + "px";
		this.cargo.style.padding = "";
		await sleep(100);
		this.cargo.style.maxHeight = "";
		this.isOpen = true;	
		if (this.parent != null)
		{
			this.ExpandParent();
		}
	}
}

for (var i = 0; i < toggles.length; ++i)
{
	toggles[i].collapsible = new Collapsible(collapsibles[i]);
	toggles[i].addEventListener("click", function()
	{
		this.collapsible.Toggle();
	});
}